# generator dat --> wczytuje plik xml i generuje daty kazdej konferencji
# konferencje trwaja od 1 do 5 dni
from random import randint
import datetime
import re


""" Zwraca string, zawierajacy odpowiednio
	sformatowana pare dat: poczatku i konca konferencji"""
def gen_rand_dates():
	year = randint(2011, 2013)
	month = randint(1, 12)
	day = randint(1, 28)
	startdate = datetime.date(year, month, day)
	
	duration = randint(1,5)
	enddate = startdate + datetime.timedelta(days=duration)
	
	return "<start>" + str(startdate) + " </start><koniec>" + str(enddate) + "</koniec>" 

	
def cena():
	c = randint(1,6)*50
	return str(c)


def limit():
	l = randint(2, 20)*5
	return str(l)
	
	
fin = file('konferencje.xml')
fout = file('konf2.xml', 'w')
text = fin.readlines()

p = re.compile('</temat></konferencja>')
	
for s in text:	
	output = p.sub('</temat>' + gen_rand_dates() + '</konfencja>', s)
	fout.write(output)

fin.close()
fout.close()
del fin
del fout
	
fin = file('warsztaty.xml')
fout = file('warsztaty_out.xml', 'w')

lines = fin.readlines()
p_cena = re.compile('654321')
p_limit = re.compile('9876')

for s in lines:
	s = p_cena.sub(cena(), s)
	s = p_limit.sub(limit(), s)
	fout.write(s)
	
fin.close()
fout.close()
del fin
del fout