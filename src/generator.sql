/* ---------------------------------------------------------
	GENERATOR
	symuluje 3 lata (= 1095 dni) dzialania firmy
	// od 1.01.2012 do 1.01.2014
------------------------------------------------------------ */

-- najpierw ustawimy ID na 0 w tabelach
dbcc checkident(Konferencje, reseed, 0)
dbcc checkident(Warsztaty, reseed, 0)
dbcc checkident(Klienci, reseed, 0)
dbcc checkident(Platnosci, reseed, 0)
dbcc checkident(PlatnosciWarsztatow, reseed, 0)
dbcc checkident(Rezerwacje, reseed, 0)
dbcc checkident(RezerwacjeWarsztatow, reseed, 0)
dbcc checkident(Dni, reseed, 0)
dbcc checkident(ProgiCenowe, reseed, 0)
dbcc checkident(Uczestnicy, reseed, 0)
dbcc checkident(ArchiwumPlatnosci, reseed, 0)
dbcc checkident(Zadania, reseed, 0)

-- tworzymy pomocnicze tabele, zawierajace przykladowe dane
-- pliki z danymi sa w formacie XML, trzymane w katalogu projektu
if object_id('ImionaUczestnikow', 'U') is not null
	drop table ImionaUczestnikow;
go

if object_id('TematyWarsztatow', 'U') is not null
	drop table TematyWarsztatow;
go

if object_id('DaneKonferencji', 'U') is not null
	drop table DaneKonferencji;
go

if object_id('DaneKlientow', 'U') is not null
	drop table DaneKlientow;
go

create table ImionaUczestnikow (
	ID int primary key identity(1,1),
	imie varchar(50),
	nazwisko varchar(50)
)

insert into ImionaUczestnikow
select
   c3.value('imie[1]','varchar(50)'),
   c3.value('nazwisko[1]','varchar(50)')
from (
   select cast(c1 as xml)
   from
      OPENROWSET (BULK 'C:\Users\Piotrek\Inf\Bazy\AGH_BD_Projekt_2013\uczestnicy.xml',SINGLE_BLOB) as T1(c1)
)as T2(c2)
cross apply c2.nodes('/ROOT/uczestnik') T3(c3)


create table TematyWarsztatow  (
	ID int primary key identity(1,1),
	temat varchar(250),
	cena money,
	limit int
)

insert into TematyWarsztatow
select
   c3.value('temat[1]','varchar(250)'),
   c3.value('cena[1]','money'),
   c3.value('limit[1]','int')
from (
   select cast(c1 as xml)
   from
      OPENROWSET (BULK 'C:\Users\Piotrek\Inf\Bazy\AGH_BD_Projekt_2013\warsztaty.xml',SINGLE_BLOB) as T1(c1)
)as T2(c2)
cross apply c2.nodes('/ROOT/warsztat') T3(c3)


create table DaneKonferencji (
	ID int primary key identity(1,1),
	temat varchar(250),
	datastartu date,
	datakonca date
)

insert into DaneKonferencji
select
   c3.value('temat[1]','varchar(250)'),
   c3.value('start[1]','date'),
   c3.value('koniec[1]','date')
from (
   select cast(c1 as xml)
   from
      OPENROWSET (BULK 'C:\Users\Piotrek\Inf\Bazy\AGH_BD_Projekt_2013\konferencje.xml',SINGLE_BLOB) as T1(c1)
)as T2(c2)
cross apply c2.nodes('/ROOT/konferencja') T3(c3)


create table  DaneKlientow (
	ID int identity(1,1),
	nazwa varchar(50),
	telefon bigint,
	email varchar(50),
	nrdomu smallint,
	ulica varchar(50),
	miasto varchar(50)
)

insert into DaneKlientow
select
   c3.value('nazwa[1]','varchar(50)'),
   c3.value('telefon[1]','bigint'),
   c3.value('email[1]','varchar(50)'),
   c3.value('nrdomu[1]','smallint'),
   c3.value('ulica[1]','varchar(50)'),
   c3.value('miasto[1]','varchar(50)')
from (
   select cast(c1 as xml)
   from
      OPENROWSET (BULK 'C:\Users\Piotrek\Inf\Bazy\AGH_BD_Projekt_2013\firmy.xml',SINGLE_BLOB) as T1(c1)
)as T2(c2)
cross apply c2.nodes('/ROOT/firma') T3(c3)


-- nim zaczniemy symulowac "ruch w interesie", stworzymy liste wszystkich konferencji
declare @iterKonf cursor,
		@IDKonferencji int,
		@Temat varchar(250),
		@DataStartu date,
		@DataKonca date,
		@ZnizkaStudencka float,
		@DataDnia date,
		@LimitMiejscDnia int,
		@CenaDnia money,
		@ProcentZnizkiProgu float

set @ZnizkaStudencka = ROUND((RAND()*3)/10, 2)
set @iterKonf = cursor for select datastartu, datakonca, temat from DaneKonferencji
open @iterKonf
fetch next from @iterKonf into @DataStartu, @DataKonca, @Temat

while @@FETCH_STATUS = 0 begin
	-- tworzymy dla tej konferencji: 1) Konferencje, 2) Dni, 3) ProgiCenowe
	-- ad1
	exec dodajKonferencje @DataStartu, @DataKonca, @Temat, @ZnizkaStudencka
	set @IDKonferencji = @@IDENTITY

	-- ad2,3
	set @ProcentZnizkiProgu =  ROUND((RAND()*2)/10, 2) -- wartosc, z ktorej startuejmy
	set @DataDnia = @DataStartu
	while @DataDnia <= @DataKonca begin
		-- tworzymy prog na podstawie rzutu moneta:
		if RAND() > 0.5 begin
			set @ProcentZnizkiProgu =  @ProcentZnizkiProgu - 0.02
			set @ProcentZnizkiProgu = (@ProcentZnizkiProgu + ABS(@ProcentZnizkiProgu))/2 -- trick, ktory ustawia 0 dla liczb ujemnych
			exec dodajProg @IDKonferencji, @DataDnia, @ProcentZnizkiProgu
		end

		-- tworzymy Dzien dla kazdego dnia...
		set @CenaDnia = cast(ROUND(RAND()*300 + 150, 0) as money)
		set @LimitMiejscDnia = ROUND(RAND()*100+50, 0)
		exec dodajDzien @IDKonferencji, @DataDnia, @LimitMiejscDnia, @CenaDnia
		
		-- kolejna iteracja:
		set @DataDnia = DATEADD(day, 1, @DataDnia)
	end

	set @ZnizkaStudencka = ROUND((RAND()*3)/10, 2)
	fetch next from @iterKonf into @DataStartu, @DataKonca, @Temat
end

close @iterKonf
deallocate @iterKonf

-- musimy rowniez utworzyc warsztaty
-- najpierw znajdziemy zakres IDKonferencji -- wiemy, ze sa po kolei, ale nie wiemy
-- od jakiej wartosci sie zaczynaja
declare @FirstIDKonf int,
		@LastIDKonf int

set @FirstIDKonf = (select top 1 IDKonferencji from Konferencje order by IDKonferencji asc)
set @LastIDKonf = (select top 1 IDKonferencji from Konferencje order by IDKonferencji desc)

-- teraz przeiterujemy po liscie warsztatow, nadajac im losowe IDKonferencji z powyzszego przedzialu
declare @iterWarsztaty cursor,
		@idKonf int,
		@tematWarsztatu varchar(250),
		@cenaWarsztatu money,
		@limitMiejscWarsztatu int,
		@dataStartuWarsztatu date,
		@dataKoncaWarsztatu date

set @idKonf = CAST(ROUND(RAND()*(@LastIDKonf - @FirstIDKonf) + @FirstIDKonf, 0) as int)
set @iterWarsztaty = cursor for select temat, cena, limit from TematyWarsztatow
open @iterWarsztaty
fetch next from @iterWarsztaty into @tematWarsztatu, @cenaWarsztatu, @limitMiejscWarsztatu

while @@FETCH_STATUS = 0 begin
	-- losujemy IDKonferencji
	set @idKonf = CAST(ROUND(RAND()*(@LastIDKonf - @FirstIDKonf) + @FirstIDKonf, 0) as int)
	-- pobieramy date startu i konca tej konferencji
	set @DataStartu = (select DataStartu from Konferencje where IDKonferencji = @idKonf)
	set @DataKonca = (select DataKonca from Konferencje where IDKonferencji = @idKonf)
	set @dataStartuWarsztatu = DATEADD(DAY, CAST(ROUND(RAND()*2, 0) as int), @DataStartu)
	if @dataStartuWarsztatu > @DataKonca set @dataStartuWarsztatu = @DataStartu
	set @dataKoncaWarsztatu = DATEADD(DAY, CAST(ROUND(RAND()*3, 0) as int), @dataStartuWarsztatu)
	if @dataKoncaWarsztatu > @DataKonca set @dataKoncaWarsztatu = @DataKonca

	-- i dodajemy odpowiedni warsztat
	exec dodajWarsztat @idKonf, @tematWarsztatu, @dataStartuWarsztatu, @dataKoncaWarsztatu, @cenaWarsztatu, @limitMiejscWarsztatu

	fetch next from @iterWarsztaty into @tematWarsztatu, @cenaWarsztatu, @limitMiejscWarsztatu
end

close @iterWarsztaty
deallocate @iterWarsztaty


-- mamy w tym momencie stworzone 1) Konferencje, 2) Dni, 3) ProgiCenowe, 4) Warsztaty.
-- teraz zaczniemy symulacje, podczas ktorej beda zachodzic nastepujace zdarzenia:
-- 1) rejestracja nowego klienta i stworzenie Platnosci
-- 2) stworzenie Platnosci przez jednego z istniejacych klientow
-- 3) wplacenie raty przez klienta (za Platnosc lub PlatnoscWarsztatow)
-- 4) dodanie listy uczestnikow przez klienta
-- 5) rejestracja uczestnika na warsztaty

exec set_date '2011-01-01' 

declare @ppNowyKlient float,
		@ppStaryKlientKonferencja float
set @ppNowyKlient = 0.08
set @ppStaryKlientKonferencja = 0.07

declare @iterKlient cursor,
		@imie varchar(50),
		@nazwisko varchar(50),
		@nazwa varchar(50),
		@telefon bigint,
		@email varchar(50),
		@nrdomu smallint,
		@ulica varchar(50),
		@miasto varchar(50),
		@czyfirma bit,
		@nip bigint,
		@regon bigint,
		@liczbaDni tinyint,
		@data date,
		@listaDoPlatnosci xml,
		@listaDoRezerwacji xml,
		@obecnyKlient int,
		@iterDniOsoby cursor,
		@dzien int,
		@osoby int

declare @dniOsoby table (
	IDDnia int,
	liczbaOsob smallint
)

declare @listaUczestnikow table (
	Imie varchar(50),
	Nazwisko varchar(50),
	NrLegitymacji int
)

declare @uczestnicyPerDzien table (
	IDDnia int,
	Imie varchar(50),
	Nazwisko varchar(50),
	NrLegitymacji int
)

set @iterKlient = cursor for select nazwa, telefon, email, nrdomu, ulica, miasto from DaneKlientow
open @iterKlient

declare @czyNowy tinyint -- jesli dodajemy nowego = 1, jesli znowu stary = 2, jesli nic = 0
declare @ileRezerwacji smallint,
		@idDniaTemp int,
		@idUczTemp int,
		@idWarsztTemp int,
		@ileOsob smallint,
		@cenaDniaTemp money,
		@cenaWarsztTemp money,
		@suma money,
		@sumaWarszt money,
		@i int

declare @checker cursor,
		@platnosc int
-- glowna petla symulacji
while dbo.get_date() < '2014-01-01' begin
	
	-- czy w tym dniu dodajemy nowego klienta?
	set @czyNowy = 0
	
	if RAND() <= @ppStaryKlientKonferencja set @czyNowy = 2
	if RAND() <= @ppNowyKlient set @czyNowy = 1
	
		
	if @czyNowy > 0 begin
		if @czyNowy = 1 begin
			-- losujemy, czy jest firma
			set @czyfirma = CAST(ROUND(RAND(), 0) as bit)
			if @czyfirma = 1 begin
				set @nip = CAST(ROUND(RAND()*10000000000 - 1, 0) as bigint)
				set @regon = CAST(ROUND(RAND()*1000000000000 - 1, 0) as bigint)
			end else begin
				set @nip = null
				set @regon = null
			end

			fetch next from @iterKlient into @nazwa, @telefon, @email, @nrdomu, @ulica, @miasto
			exec dodajKlienta @nazwa, @telefon, @email, @czyfirma, @nip, @regon, @nrdomu, @ulica, @miasto
			set @obecnyKlient = @@IDENTITY	-- bedzie potrzebny przy dodawaniu platnosci
			--set @obecnyKlient = (select IDKlienta from Klienci where (NazwaKlienta = @nazwa) and (NrTelefonu = @telefon))
		end else
			-- jesli tu jestesmy, to ktorys ze starych klientow bedzie dodawal konferencje
			set @obecnyKlient = (select top 1 IDKlienta from Klienci order by NEWID()) -- wybor losowego

	
		if @obecnyKlient is null continue
		
		-- klient od razu rejestruje sie na konferencje. Wybiera najdalsza konferencje sposrod
		-- odleglych o co najwyzej 4 tygodnie:
		set @idKonferencji = (select top 1 IDKonferencji from Konferencje where DataStartu < DATEADD(week, 4, dbo.get_date()) order by DataStartu desc)
		if @IDKonferencji is null set @IDKonferencji = (select top 1 IDKonferencji from Konferencje order by DataStartu desc)
		set @liczbaDni = (select count(*) from Dni where IDKonferencji = @idKonferencji)
		
		-- generujemy odpowiednia liczbe dnio-osob
		set @suma = 0
		set @sumaWarszt = 0
		set @data = (select DataStartu from Konferencje where IDKonferencji = @IDKonferencji)
		while @data <= (select DataKonca from Konferencje where IDKonferencji = @IDKonferencji) begin
			
			set @idDniaTemp = (select IDDnia from Dni where (IDKonferencji = @idKonferencji) and (Data = @data))
			set @ileOsob = CAST(ROUND(RAND()*10 + 1, 0) as smallint)
			set @cenaDniaTemp = (select Cena from Dni where IDDnia = @idDniaTemp)
			set @i = @ileOsob
			
			while @i > 0 begin
				insert into Uczestnicy (IDKlienta, Imie, Nazwisko)
				values (@obecnyKlient, (select top 1 imie from ImionaUczestnikow order by NEWID()), (select top 1 imie from ImionaUczestnikow order by NEWID()))
				
				set @idUczTemp = @@IDENTITY
				insert into Rezerwacje values (@idUczTemp, @idDniaTemp)
				
				---- ZMIANA:
				-- uczestnik moze tez sie w tym momencie zarejestrowac na warsztat (0, 1 lub 2)
				-- wybieramy losowy warsztat dla tej konferencji
				if RAND() < 0.8 begin
					set @idWarsztTemp = (select top 1 IDWarsztatu from Warsztaty where IDKonferencji = @IDKonferencji order by NEWID())
					set @cenaWarsztTemp = (select CenaWarsztatu from Warsztaty where IDWarsztatu = @idWarsztTemp)
					exec rezerwujWarsztat @idUczTemp, @idWarsztTemp
					set @sumaWarszt = @sumaWarszt + @cenaWarsztTemp
				end
				if RAND() < 0.7 begin
					set @idWarsztTemp = (select top 1 IDWarsztatu from Warsztaty where IDKonferencji = @IDKonferencji order by NEWID())
					set @cenaWarsztTemp = (select CenaWarsztatu from Warsztaty where IDWarsztatu = @idWarsztTemp)
					exec rezerwujWarsztat @idUczTemp, @idWarsztTemp
					set @sumaWarszt = @sumaWarszt + @cenaWarsztTemp
				end

				set @i = @i - 1
			end
			
			set @suma = @suma + @cenaDniaTemp*@ileOsob
			set @data = DATEADD(day, 1, @data)
		end
		
		-- dodajemy platnosc:
		insert into Platnosci values (@obecnyKlient, @IDKonferencji, dbo.get_date(), @suma, 0, 0)
		insert into PlatnosciWarsztatow values (@obecnyKlient, @IDKonferencji, @sumaWarszt, 0)
		

	end -- koniec dodawania klientow-warsztatow/platnosci

	-- co 10 dni oplacane jest wszystko, co nalezy oplacic:
	if datepart(dayofyear, dbo.get_date()) % 10 = 0 begin
		update Platnosci
		set Zaplacone = dbo.oplac(IDPlatnosci)
		where (Zaplacone < Kwota-KwotaZnizki)

		update PlatnosciWarsztatow
		set Zaplacone = dbo.oplacWarsztat(IDPlatnosciWarsztatu)
		where (Zaplacone < Kwota)
	end

	-- co 100 dni wykonywane jest sprawdzenie kto, jak, kiedy i dlaczego
	if datepart(dayofyear, dbo.get_date()) % 100 = 0 begin
		
		set @checker = cursor for (select IDPlatnosci from Platnosci)
		open @checker
		fetch next from @checker into @platnosc

		while @@FETCH_STATUS != 0 begin
			update Platnosci
			set Kwota = Kwota
			where IDPlatnosci = @platnosc  -- trick powodujacy odpalenie wczesniej zablokowanego
										   -- triggera na wszystkich platnosciach
			fetch next from @checker into @platnosc
		end
		close @checker
		
		-- sprawdzenie terminow (nie mozemy wykonywac zbyt czesto z powodow wydajnosciowych --
		-- (ograniczenie srodowiska wydajnosciowego) w normalnych warunkach wykonywanie raz dziennie
		-- zuzywa minimalna ilosc zasobow
		exec usunPlatnosciPoTerminie
	end
	

	-- kolejny dzien symulacji:
	exec inc_date
end

close @iterKlient
deallocate @iterKlient
deallocate @checker