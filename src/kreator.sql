/* ------------------------
	Skrypt tworzacy baze 
   ------------------------ */
   
if db_id('FirmaKonf') is not null
	drop database FirmaKonf;
go

use master
go

create database FirmaKonf
go

use FirmaKonf
go 

--------------------------------------------------------------------
-- TWORZENIE TABEL
--------------------------------------------------------------------

create table Klienci(
	IDKlienta int identity(1,1) primary key not null,
	NazwaKlienta varchar(50) not null,
	NrTelefonu bigint not null, --unique,
	AdresEmail varchar(50) not null check (AdresEmail like '%@%') unique,
	CzyFirma bit default 0,
	NIP bigint null, -- check (NIP between 1000000000 and 9999999999),
	Regon bigint null,
	NrBudynku smallint null,
	Ulica varchar(50) null,
	Miasto varchar(50) null
)

create table Uczestnicy (
	IDUczestnika int identity(1,1) primary key not null,
	Imie varchar(50) null,
	Nazwisko varchar(50) null,
	NrLegitymacjiStudenckiej bigint null,
	IDKlienta int foreign key references Klienci(IDKlienta) not null
)


create table Konferencje (
	IDKonferencji int identity(1,1) primary key not null,
	DataStartu date not null,
	DataKonca date not null,
	Temat varchar(250) null,
	ZnizkaStudencka float default 0 check (ZnizkaStudencka >= 0 and ZnizkaStudencka <= 1)
)

create table Dni (
	IDDnia int identity(1,1) primary key not null,
	IDKonferencji int foreign key references Konferencje(IDKonferencji) not null,
	Data date not null,
	LimitMiejsc int not null check (LimitMiejsc > 0),
	Cena money not null
)

create table Rezerwacje (
	IDRezerwacji int identity(1,1) primary key not null,
	IDUczestnika int foreign key references Uczestnicy(IDUczestnika) not null,
	IDDnia int foreign key references Dni(IDDnia) not null
)

create table Platnosci (
	IDPlatnosci int identity(1,1) primary key not null,
	IDKlienta int foreign key references Klienci(IDKlienta) not null,
	IDKonferencji int foreign key references Konferencje(IDKonferencji) not null,
	DataZaksiegowania date not null,
	Kwota money not null,
	Zaplacone money default 0,
	KwotaZnizki money default 0
)	

create table ProgiCenowe (
	IDProgu int identity(1,1) primary key not null,
	IDKonferencji int foreign key references Konferencje(IDKonferencji) not null,
	DataGraniczna date not null,
	ProcentZnizki float default 0 check (ProcentZnizki >= 0 and ProcentZnizki <= 1)
)

create table Warsztaty (
	IDWarsztatu int identity(1,1) primary key not null,
	IDKonferencji int foreign key references Konferencje(IDKonferencji) not null,
	Nazwa varchar(50) null,
	DataStartu date not null,
	DataKonca date not null,
	CenaWarsztatu money not null,
	LimitMiejsc int not null check (LimitMiejsc > 0)
)

create table RezerwacjeWarsztatow (
	IDRezerwacjiWarsztatu int identity(1,1) primary key not null,
	IDUczestnika int foreign key references Uczestnicy(IDUczestnika) not null,
	IDWarsztatu int foreign key references Warsztaty(IDWarsztatu) not null
)

create table PlatnosciWarsztatow (
	IDPlatnosciWarsztatu int identity(1,1) primary key not null,
	IDKlienta int foreign key references Klienci(IDKlienta) not null,
	IDKonferencji int foreign key references Konferencje(IDKonferencji) not null,
	Kwota money not null,
	Zaplacone money default 0
)

create table Zadania (
	IDZadania int identity(1,1) primary key not null,
	IDPlatnosci int foreign key references Platnosci(IDPlatnosci) not null,
	TypZadania varchar(8) not null check (TypZadania in ('zaliczka', 'osoby', 'zaplata'))
)

create table ArchiwumPlatnosci (
	IDPlatnosciArch int identity(1,1) primary key not null,
	IDKlienta int foreign key references Klienci(IDKlienta) not null,
	IDKonferencji int foreign key references Konferencje(IDKonferencji) not null,
	Kwota money not null,
	Zaplacone money not null
)

create table ObecnaData (
	obData date primary key
)

------------------------------------------------------------------------------
-- Funkcja i procedury potrzebne do dzialania pozostalych procedur -- sluza
-- do przesloniecia daty systemowej celem wykoniania symulacji kolejnych dni
------------------------------------------------------------------------------
if object_id ('get_date', 'FN') is not null
	drop function get_date;
go

create function get_date()
returns date
as begin
	return (select top 1 obData from ObecnaData)
end
go

if object_id ('inc_date', 'P') is not null
	drop procedure inc_date;
go

create procedure inc_date
as begin
	update ObecnaData
	set obData = DATEADD(day, 1, obData)
end
go

if object_id ('set_date', 'P') is not null
	drop procedure set_date;
go

create procedure set_date ( @newDate date )
as begin
	delete from ObecnaData
	insert into ObecnaData values (@newDate)
end
go

--------------------------------------------------------------------
-- TWORZENIE PROCEDUR
--------------------------------------------------------------------

/* ------------------------------------------------------
	Dodawanie/usuwanie/modyfikacja danych  klienta
   ------------------------------------------------------ */
 
-- dodaje do tabeli Klienci nowy rekord:
if object_id ( 'dodajKlienta', 'P' ) is not null 
    drop procedure dodajKlienta;
go

create procedure dodajKlienta (
	@NazwaKlienta varchar(50),
	@NrTelefonu bigint,
	@AdresEmail varchar(50),
	@CzyFirma bit,
	@NIP bigint = null,
	@Regon bigint = null,
	@NrBudynku smallint = null,
	@Ulica varchar(50) = null,
	@Miasto varchar(50) = null
) as
begin
	declare @Klient varchar(50);
	set @Klient = (select NazwaKlienta from Klienci where NazwaKlienta = @NazwaKlienta)
	if @Klient is null
		insert into Klienci
		values (@NazwaKlienta, @NrTelefonu, @AdresEmail, @CzyFirma, @NIP, @Regon, @NrBudynku, @Ulica, @Miasto)
end
go


-- usuwa klienta oraz wszystkie platnosci(Warsztatow), rezerwacje(Warsztatow), zadadnia i uczestnikow, ktorzy do niego przynaleza:
if object_id ( 'usunKlienta', 'P' ) is not null 
    drop procedure usunKlienta;
go

create procedure usunKlienta (
	@IDKlienta int
) as
begin
	begin transaction usuwanieKlienta;
		delete from Zadania
		where Zadania.IDPlatnosci = (select Platnosci.IDPlatnosci from Platnosci where Platnosci.IDKlienta = @IDKlienta)

		if (@@ERROR != 0)
		begin
			raiserror('Nie udalo sie usunac Zadan dla tego klienta', 1,1)
			rollback transaction
		end

		delete from Platnosci
		where Platnosci.IDKlienta = @IDKlienta

		if (@@ERROR != 0)
		begin
			raiserror('Nie udalo sie usunac Platnosci dla tego klienta', 1,1)
			rollback transaction
		end

		delete from PlatnosciWarsztatow
		where PlatnosciWarsztatow.IDKlienta = @IDKlienta

		if (@@ERROR != 0)
		begin
			raiserror('Nie udalo sie usunac PlatnosciWarsztatow dla tego klienta', 1,1)
			rollback transaction
		end

		delete from Rezerwacje
		where Rezerwacje.IDUczestnika = (select Uczestnicy.IDUczestnika from Uczestnicy where Uczestnicy.IDKlienta = @IDKlienta)

		if (@@ERROR != 0)
		begin
			raiserror('Nie udalo sie usunac Rezerwacji dla tego klienta', 1,1)
			rollback transaction
		end

		delete from RezerwacjeWarsztatow
		where RezerwacjeWarsztatow.IDUczestnika = (select Uczestnicy.IDUczestnika from Uczestnicy where Uczestnicy.IDKlienta = @IDKlienta)

		if (@@ERROR != 0)
		begin
			raiserror('Nie udalo sie usunac RezerwacjiWarsztatow dla tego klienta', 1,1)
			rollback transaction
		end

		delete from Uczestnicy
		where Uczestnicy.IDKlienta = @IDKlienta

		if (@@ERROR != 0)
		begin
			raiserror('Nie udalo sie usunac Uczestnikow dla tego klienta', 1,1)
			rollback transaction
		end

		delete from Klienci
		where Klienci.IDKlienta = @IDKlienta

		if (@@ERROR != 0)
		begin
			raiserror('Nie udalo sie usunac tego Klienta', 1,1)
			rollback transaction
		end

	commit transaction usuwanieKlienta;
end
go


-- modyfikacja danych klienta
if object_id ( 'modyfikujDaneKlienta', 'P' ) is not null 
    drop procedure modyfikujDaneKlienta;
go

create procedure modyfikujDaneKlienta (
	@IDKlienta int,
	@NrTelefonu int = null,
	@AdresEmail varchar(50) = null,
	@NIP int = null,
	@Regon int = null,
	@NrBudynku smallint = null,
	@Ulica varchar(50) = null,
	@Miasto varchar(50) = null
) as
begin
	declare @NowyTelefon int,
			@NowyEmail varchar(50),
			@NowyNIP int,
			@NowyRegon int,
			@NowyNrBudynku smallint,
			@NowaUlica varchar(50),
			@NoweMiasto varchar(50)
	
	if (@NrTelefonu is not null) set @NowyTelefon = @NrTelefonu
	else set @NowyTelefon = (select NrTelefonu from Klienci where IDKlienta = @IDKlienta)
	if (@AdresEmail is not null) set @NowyEmail = @AdresEmail
	else set @NowyEmail = (select AdresEmail from Klienci where IDKlienta = @IDKlienta)
	if (@NIP is not null) set @NowyNIP = @NIP
	else set @NowyNIP = (select NIP from Klienci where IDKlienta = @IDKlienta)
	if (@Regon is not null) set @NowyRegon = @Regon
	else set @NowyRegon = (select Regon from Klienci where IDKlienta = @IDKlienta)
	if (@NrBudynku is not null) set @NowyNrBudynku = @NrBudynku
	else set @NowyNIP = (select NrBudynku from Klienci where IDKlienta = @IDKlienta)
	if (@Ulica is not null) set @NowaUlica = @Ulica
	else set @NowaUlica = (select Ulica from Klienci where IDKlienta = @IDKlienta)
	if (@Miasto is not null) set @NoweMiasto = @Miasto
	else set @NoweMiasto = (select Miasto from Klienci where IDKlienta = @IDKlienta)

	update Klienci
	set NrTelefonu = @NowyTelefon,
		AdresEmail = @NowyEmail,
		NIP = @NowyNIP,
		Regon = @NowyRegon,
		NrBudynku = @NowyNrBudynku,
		Ulica = @Ulica,
		Miasto = @NoweMiasto
	where IDKlienta = @IDKlienta

end
go


/* ---------------------------------------------
   Dodawanie/usuwanie/modyfikowanie konferencji
   Dodawanie dni do konferencji
   --------------------------------------------- */

--dodajeKonferencje
if object_id ( 'dodajKonferencje', 'P' ) is not null 
    drop procedure dodajKonferencje;
go

create procedure dodajKonferencje (
	@DataStartu date,
	@DataKonca date,
	@Temat varchar(250) = null,
	@ZnizkaStudencka float = null
) as
begin
	insert into Konferencje
	values (@DataStartu, @DataKonca, @Temat, @ZnizkaStudencka)
end
go


-- usuwanie konferencji oraz wszystkich do niej przynaleznych dni, warsztatow i progow cenowych
-- da sie usunac konferencje tylko jesli nie ma zadnych uczestnikow na nia zarezerwowanych
-- (albo na towarzyszace jej warsztaty)
if object_id ( 'usunKonferencje', 'P' ) is not null 
    drop procedure usunKonferencje;
go

create procedure usunKonferencje (
	@IDKonferencji int
) as
begin
	if  exists (select IDPlatnosci from Platnosci where Platnosci.IDKonferencji = @IDKonferencji) or
		exists (select IDPlatnosciWarsztatu from PlatnosciWarsztatow where PlatnosciWarsztatow.IDKonferencji = @IDKonferencji) or
		exists (select IDUczestnika from Uczestnicy
			where (IDUczestnika in
				(select IDUczestnika from Rezerwacje
				 where Rezerwacje.IDDnia =
					(select Dni.IDDnia from Dni
					 where Dni.IDKonferencji =
						(select IDKonferencji from Konferencje where IDKonferencji = @IDKonferencji)))) or (
			(IDUczestnika in (select IDUczestnika from RezerwacjeWarsztatow
								where IDWarsztatu = (select IDWarsztatu from Warsztaty
									where IDKonferencji = (select IDKonferencji from Konferencje
										where IDKonferencji = @IDKonferencji))))))
	begin
		raiserror('Nie mozna usunac konferencji przed zamknieciem wszystkich rezerwacji i transakcji', 1,1)
	end
	else
	begin
		begin transaction

			delete from ProgiCenowe
			where IDKonferencji = @IDKonferencji

			if (@@ERROR != 0) begin rollback transaction; raiserror('Nie udalo sie usunac ProgowCenowych', 1,1) end

			delete from Dni
			where IDKonferencji = @IDKonferencji

			if (@@ERROR != 0) begin rollback transaction; raiserror('Nie udalo sie usunac Dni', 1,1) end

			delete from Warsztaty
			where IDKonferencji = @IDKonferencji

			if (@@ERROR != 0) begin rollback transaction; raiserror('Nie udalo sie usunac Warsztatow', 1,1) end

			delete from Konferencje
			where IDKonferencji = @IDKonferencji

			if (@@ERROR != 0) begin rollback transaction; raiserror('Nie udalo sie usunac tej Konferencji', 1,1) end

		commit transaction
	end
end
go


-- modyfikacja konferencji: mozna zmieniac wszystkie dane, ale tylko do tygodnia przed konferencja
if object_id ( 'modyfikujKonferencje', 'P' ) is not null 
    drop procedure modyfikujKonferencje;
go

create procedure modyfikujKonferencje (
	@IDKonferencji int,
	@DataStartu date = null,
	@DataKonca date = null,
	@Temat varchar(250) = null
) as
begin
	declare @varDataStartu date
	set @varDataStartu = (select DataStartu from Konferencje where IDKonferencji = @IDKonferencji)

	if @varDataStartu > (DATEADD(DAY, 7, dbo.get_date()))
	begin
		if @DataStartu is null
			set @DataStartu = (select DataStartu from Konferencje where IDKonferencji = @IDKonferencji)
		if @DataKonca is null
			set @DataKonca = (select DataKonca from Konferencje where IDKonferencji = @IDKonferencji)
		if @Temat is null
			set @Temat = (select Temat from Konferencje where IDKonferencji = @IDKonferencji)
		

		update Konferencje
		set DataStartu = @DataStartu,
			DataKonca = @DataKonca,
			Temat = @Temat
		where IDKonferencji = @IDKonferencji
	end
	else
		raiserror('Nie mozna juz modyfikowac tej konferencji', 1,1)
end
go


-- dodawanie dnia do danej konferencji
if object_id ( 'dodajDzien', 'P' ) is not null 
    drop procedure dodajDzien;
go

create procedure dodajDzien (
	@IDKonferencji int,
	@Data date,
	@LimitMiejsc int,
	@Cena money
) as
begin
	insert into Dni
	values (@IDKonferencji, @Data, @LimitMiejsc, @Cena)
end
go


-- usuwanie dnia; mozna to zrobic tylko, jesli nie ma na niego zadnych rezerwacji
if object_id ( 'usunDzien', 'P' ) is not null 
    drop procedure usunDzien;
go

create procedure usunDzien (
	@IDDnia int
) as
begin
	if exists (select * from Rezerwacje where IDDnia = @IDDnia)
		raiserror('Nie mozna usunac Dnia, na ktory sa rezerwacje', 1,1)
	else
		delete from Dni
		where IDDnia = @IDDnia
end
go


/* ------------------------------------------
   Dodawanie/usuwanie/modyfikacja warsztatow
   ------------------------------------------ */

-- dodaje Warsztat
if object_id ( 'dodajWarsztat', 'P' ) is not null 
    drop procedure dodajWarsztat;
go

create procedure dodajWarsztat (
	@IDKonferencji int,
	@Nazwa varchar(50) = null,
	@DataStartu date,
	@DataKonca date,
	@CenaWarsztatu money,
	@LimitMiejsc int
) as
begin
	insert into Warsztaty
	values (@IDKonferencji, @Nazwa, @DataStartu, @DataKonca, @CenaWarsztatu, @LimitMiejsc)
end
go

-- usuwa warsztat, jesli nie ma na niego zadnych rezerwacji.
-- NIE jest wolane przez usunKonferencje podczas usuwaniaKonferencji
if object_id ( 'usunWarsztat', 'P' ) is not null 
    drop procedure usunWarsztat;
go

create procedure usunWarsztat (
	@IDWarsztatu int
) as
begin
	if exists (select * from RezerwacjeWarsztatow where RezerwacjeWarsztatow.IDWarsztatu = @IDWarsztatu)
		raiserror('Nie mozna usunac tego warszatu, bo sa na niego rezerwacje', 1,1)
	else begin
		delete from Warsztaty
		where IDWarsztatu = @IDWarsztatu
	end
end
go

-- zmienia dane warsztatu (nie mozna zmienic przypisania do konferencji)
if object_id ( 'modyfikujWarsztat', 'P' ) is not null 
    drop procedure modyfikujWarsztat;
go

create procedure modyfikujWarsztat (
	@IDWarsztatu int,
	@Nazwa varchar(50) = null,
	@DataStartu date = null,
	@DataKonca date = null,
	@CenaWarsztatu money = null,
	@LimitMiejsc int = null
) as
begin
	declare @NowaNazwa varchar(50),
			@NowyStart date,
			@NowyKoniec date,
			@NowaCena money,
			@NowyLimit int

	if @Nazwa is not null set @NowaNazwa = @Nazwa
	else set @NowaNazwa = (select Nazwa from Warsztaty where IDWarsztatu = @IDWarsztatu)
	if @DataStartu is not null set @NowyStart = @DataStartu
	else set @NowyStart = (select DataStartu from Warsztaty where IDWarsztatu = @IDWarsztatu)
	if @DataKonca is not null set @NowyKoniec = @DataKonca
	else set @NowyKoniec = (select DataKonca from Warsztaty where IDWarsztatu = @IDWarsztatu)
	if @CenaWarsztatu is not null set @NowaCena = @CenaWarsztatu
	else set @NowaCena = (select CenaWarsztatu from Warsztaty where IDWarsztatu = @IDWarsztatu)
	if @LimitMiejsc is not null set @NowyLimit = @LimitMiejsc
	else set @NowyLimit = (select LimitMiejsc from Warsztaty where IDWarsztatu = @IDWarsztatu)

	update Warsztaty
	set Nazwa = @NowaNazwa,
		DataStartu = @NowyStart,
		DataKonca = @NowyKoniec,
		CenaWarsztatu = @NowaCena,
		LimitMiejsc = @NowyLimit
	where IDWarsztatu = @IDWarsztatu
end
go

/* ---------------------------------------------------------------------------------------------------------------
   dodawanie/usuwanie/modyfikacja progow cenowych
   --------------------------------------------------------------------------------------------------------------- */

-- dodaje nowy prog do danej konferencji
if object_id ( 'dodajProg', 'P' ) is not null 
    drop procedure dodajProg;
go

create procedure dodajProg (
	@IDKonferencji int,
	@DataGraniczna date,
	@ProcentZnizki float
) as
begin
	insert into ProgiCenowe
	values (@IDKonferencji, @DataGraniczna, @ProcentZnizki)
end
go


-- usuwa prog cenowy z danej konferencji
if object_id ( 'usunProg', 'P' ) is not null 
    drop procedure usunProg;
go

create procedure usunProg (
	@IDProgu int
) as
begin
	delete from ProgiCenowe
	where IDProgu = @IDProgu
end
go

-- zmienia date graniczna w progu
if object_id ( 'zmienDateGraniczna', 'P' ) is not null 
    drop procedure zmienDateGraniczna;
go

create procedure zmienDateGraniczna (
	@IDProgu int,
	@DataGraniczna date
) as
begin
	update ProgiCenowe
	set DataGraniczna = @DataGraniczna
	where IDProgu = @IDProgu
end
go

/* -----------------------------------------
   dodawanie/usuwanie/modyfikacja platnosci
   ----------------------------------------- */


-- dodaje platnosc, wylicza kwote do zaplacenia na podstawie par (Dzien, IloscOsob)
-- oprocz tego tworzy uzytkownikow Anonimow i odopowiednie dla nich rezerwacje
if object_id ( 'dodajPlatnosc', 'P' ) is not null 
    drop procedure dodajPlatnosc;
go

create procedure dodajPlatnosc (
	@IDKlienta int,
	@IDKonferencji int,
	@ListaOsob xml
) as
begin
	begin transaction
		-- najpierw musimy sparsowac xmla i utworzyc kursor do iterowania
		-- xml postaci: <ROOT>   <pair> <dzien></dzien> <ileosob></ileosob> </pair>   </ROOT>
		declare @iterator cursor,
				@dzien int,
				@ileosob int,
				@suma int,
				@cenaDnia money,
				@idUczestnika int,
				@ileRezerwacji int,
				@limitMiejsc int,
				@i int
	
		set @suma = 0
		set @iterator = cursor for (select T.N.value('dzien[1]', 'int') as Dzien, T.N.value('ileosob[1]', 'int') as IleOsob
									from @ListaOsob.nodes('ROOT/pair') as T(N))
		if @@ERROR != 0 begin raiserror('Blad w parsowaniu xmla', 1,1); rollback transaction end

		open @iterator
		fetch next from @iterator into @dzien, @ileosob
		while @@FETCH_STATUS = 0 begin
			-- sprawdzamy, czy nie przekroczono limitu miejsc na dany dzien
			set @ileRezerwacji = (select count(*) from Rezerwacje where IDDnia = @dzien) + @ileosob
			set @limitMiejsc = (select LimitMiejsc from Dni where IDDnia = @dzien)
			if @ileRezerwacji > @limitMiejsc begin raiserror('Przekroczono limit miejsc na dzien', 1,1); rollback transaction end

			-- dla kazdej pary 1. wyliczamy naleznosc i dodajemy do sumy
			set @cenaDnia = (select Cena from Dni where Dni.IDDnia = @dzien)
			set @suma = @suma + (@cenaDnia*@ileosob)
			-- dla kazdej pary tworzymy @ileosob rezerwacji oraz uczestnikow
			set @i = @ileosob
			while @i > 0 begin
				insert into Uczestnicy (IDKlienta) values (@IDKlienta)
				set @idUczestnika = @@IDENTITY
				insert into Rezerwacje values (@idUczestnika, @dzien)
				set @i = @i - 1
			end
			if @@ERROR != 0 begin raiserror('Blad w parsowaniu xmla', 1,1); rollback transaction end
			fetch next from @iterator into @dzien, @ileosob
		end

		if @@ERROR != 0 begin raiserror('Blad w parsowaniu xmla', 1,1); rollback transaction end
		-- teraz musimy uwzglednic progi cenowe // doliczymy ew. kwote do znizki
		declare @ProcentZnizki float,
				@KwotaZnizki money
		set @ProcentZnizki = (select top 1 ProcentZnizki from ProgiCenowe where DataGraniczna >= dbo.get_date() order by DataGraniczna asc)
		set @KwotaZnizki = @suma * @ProcentZnizki
		-- jesli wszystko poszlo zgodnie z planem, zapisujemy Platnosc
		insert into Platnosci values (@IDKlienta, @IDKonferencji, dbo.get_date(), @suma, 0, @KwotaZnizki)
		
		close @iterator
		deallocate @iterator
	commit transaction
end
go

-- usuwa platnosc oraz wszystkie przynalezne do niej rezerwacje, uczestnikow i zadania
-- archiwizuje dane platnosci w tabeli ArchiwumPlatnosci
if object_id ( 'usunPlatnosc', 'P' ) is not null 
    drop procedure usunPlatnosc;
go

create procedure usunPlatnosc (
	@IDPlatnosci int
) as
begin
	begin transaction
		-- najpierw usuwamy rezerwacje i  uczestnikow przynaleznych do tej konferencji i do tego klienta
		declare @IDKlienta int,
				@IDKonferencji int

		set @IDKlienta = (select IDKlienta from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @IDKonferencji = (select IDKonferencji from Platnosci where IDPlatnosci = @IDPlatnosci)

		delete from Rezerwacje
		where (Rezerwacje.IDUczestnika in (select IDUczestnika from Uczestnicy where Uczestnicy.IDKlienta = @IDKlienta)) and
			  (Rezerwacje.IDDnia in (select IDDnia from Dni where Dni.IDKonferencji = @IDKonferencji))
		
		if @@ERROR != 0 begin raiserror('Nie udalo sie usunac Rezerwacji', 1,1); rollback transaction end

		delete from Zadania
		where IDPlatnosci = @IDPlatnosci

		if @@ERROR != 0 begin raiserror('Nie udalo sie usunac Zadan', 1,1); rollback transaction end

		-- dodajemy rekord do tabeli ArchiwumPlatnosci
		declare @Kwota money,
				@Zaplacone money,
				@Znizka money

		set @Kwota = (select Kwota from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @Zaplacone = (select Zaplacone from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @Znizka = (select KwotaZnizki from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @Kwota = @Kwota - @Znizka
		if @@ERROR != 0 begin raiserror('Nie udalo sie usunac Zadan', 1,1); rollback transaction end

		insert into ArchiwumPlatnosci values (@IDKlienta, @IDKonferencji, @Kwota, @Zaplacone)
		if @@ERROR != 0 begin raiserror('Nie udalo sie usunac Zadan', 1,1); rollback transaction end

	commit transaction
end
go

/* ------------------------------------------------------
   dodawanie/usuwanie/modyfikacja platnosci za warsztaty
   ------------------------------------------------------ */

-- dodaje platnosc za warsztat (z zerowa kwota -- uczestnicy rejestruja sie pozniej)
if object_id ('dodajPlatnoscWarsztatu', 'P') is not null
	drop procedure dodajPlatnoscWarsztatu;
go

create procedure dodajPlatnoscWarsztatu (
	@IDKlienta int,
	@IDKonferencji int
) as
begin
	insert into PlatnosciWarsztatow
	values (@IDKlienta, @IDKonferencji, 0, 0)
end
go


-- usuwa platnosc warsztatow oraz wszystkie RezerwacjeWarsztatow do niej przynalezne
if object_id ('usunPlatnoscWarsztatu', 'P') is not null
	drop procedure usunPlatnoscWarsztatu;
go

create procedure usunPlatnoscWarsztatu (
	@IDPlatnosciWarsztatu int
) as
begin
	begin transaction
		declare @IDKlienta int,
				@IDKonferencji int

		set @IDKlienta = (select IDKlienta from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)
		set @IDKonferencji = (select IDKonferencji from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)
		
		-- usuwamy RezerwacjeWarsztatow przynalezne do odp. Klienta i Konferencji
		delete from RezerwacjeWarsztatow
		where (RezerwacjeWarsztatow.IDWarsztatu in (select IDWarsztatu from Warsztaty where IDKonferencji = @IDKonferencji))
			  and (RezerwacjeWarsztatow.IDUczestnika in (select IDUczestnika from Uczestnicy where IDKlienta = @IDKlienta))
		
		if @@ERROR != 0 begin raiserror('Blad!!!', 1,1); rollback transaction end

	commit transaction
end
go


-- rezerwuje uczestnika na dany warsztat i dodaje kwote do PlatnosciWarsztatow
if object_id ('rezerwujWarsztat', 'P') is not null
	drop procedure rezerwujWarsztat;
go

create procedure rezerwujWarsztat (
	@IDUczestnika int,
	@IDWarsztatu int
) as
begin
	declare @IDKonferencji int,
			@IDKlienta int,
			@IDPlatnosciWarsztatu int,
			@Cena money

	set @IDKonferencji = (select IDKonferencji from Warsztaty where IDWarsztatu = @IDWarsztatu)
	set @IDKlienta = (select IDKlienta from Uczestnicy where IDUczestnika = @IDUczestnika)
	set @IDPlatnosciWarsztatu = (select IDPlatnosciWarsztatu from PlatnosciWarsztatow
									where (IDKlienta = @IDKlienta) and (IDKonferencji = @IDKonferencji))
	set @Cena = (select CenaWarsztatu from Warsztaty where IDWarsztatu = @IDWarsztatu)

	begin transaction
		insert into RezerwacjeWarsztatow
		values (@IDUczestnika, @IDWarsztatu)

		if @@ERROR != 0 begin raiserror('Nie udalo sie zarezerwowac warsztatu', 1,1); rollback transaction end

		update PlatnosciWarsztatow
		set Kwota = Kwota + @Cena
		where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu

		if @@ERROR != 0 begin raiserror('Nie udalo sie zarezerwowac warsztatu', 1,1); rollback transaction end
	commit transaction
end
go

/* ---------------------------------------------------------------------
   wplacanie raty -- zwiekszamy pole 'zaplacone' w Platnosci(Warsztatow)
   --------------------------------------------------------------------- */

-- jedna wplata:
if object_id ('zrobWplate', 'P') is not null
	drop procedure zrobWplate;
go

create procedure zrobWplate (
	@IDPlatnosci int,
	@WplaconaKwota int
) as
begin
	update Platnosci
	set Zaplacone = Zaplacone + @WplaconaKwota
	where IDPlatnosci = @IDPlatnosci
end
go


-- jedna wplata na Warsztaty:
if object_id ('zrobWplateWarsztat', 'P') is not null
	drop procedure zrobWplateWarsztat;
go

create procedure zrobWplateWarsztat (
	@IDPlatnosciWarsztatu int,
	@WplaconaKwota int
) as
begin
	update PlatnosciWarsztatow
	set Zaplacone = Zaplacone + @WplaconaKwota
	where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu
end
go


/* --------------------------------
   przetwarzanie listy uczestnikow
   -------------------------------- */

-- procedura dostaje liste (IDDnia, Imie, Nazwisko, NrLegitymacjiStudenckiej) 
-- na tej podstawie wypelnia tabele Uczestnicy, ewentualnie scalając paru
-- Anonimow w jeden rekord
if object_id ('tworzUczestnikow', 'P') is not null
	drop procedure tworzUczestnikow;
go

create procedure tworzUczestnikow (
	@IDKlienta int,
	@IDKonferencji int,
	@ListaUczestnikow xml
) as
begin
	begin transaction
		-- najpierw parsujemy XML'a w postaci:
		-- <ROOT> <uczestnik> <dzien></dzien> <imie></imie> <nazwisko></nazwisko> <nrlegitymacji></nrlegitymacji> </uczestnik> </ROOT>
		declare @uczestnik int,
				@uczestnikWlasciwy int,
				@dzien int,
				@imie varchar(50),
				@nazwisko varchar(50),
				@nrlegitymacji int,
				@cenaDnia money,
				@znizkaStudencka float,
				@kwotaZnizki money,
				@iterator cursor

		set @iterator = cursor for (select T.N.value('dzien[1]', 'int') as Dzien, T.N.value('imie[1]', 'varchar(50)') as Imie,
									T.N.value('nazwisko[1]', 'varchar(50)') as Nazwisko, T.N.value('nrlegitymacji[1]', 'int') as NrLegitymacji
									from @ListaUczestnikow.nodes('ROOT/uczestnik') as T(N))
		if @@ERROR != 0 begin raiserror('Blad w parsowaniu xmla', 1,1); rollback transaction end
		
		open @iterator
		fetch next from @iterator into @dzien, @imie, @nazwisko, @nrlegitymacji
		while @@FETCH_STATUS = 0 begin
			-- bierzemy pierwszego goscia z listy uczestnikow, ktory ma same nulle i "naszego" klienta
			-- oraz jest zarezerwowany na "nasza" konferencje
			set @uczestnik = (select top 1 IDUczestnika from Uczestnicy 
								where (IDKlienta = @IDKlienta) and
								(IDUczestnika in (select IDUczestnika from Rezerwacje where IDDnia in (select IDDnia from Dni where IDKonferencji = @IDKonferencji))) and
								(Imie is null) and (Nazwisko is null) and (NrLegitymacjiStudenckiej is null))
			-- jesli trzeba go polaczyc z innym uczestnikiem:
			set @uczestnikWlasciwy = (select top 1 IDUczestnika from Uczestnicy 
								where (IDKlienta = @IDKlienta) and
								(IDUczestnika in (select IDUczestnika from Rezerwacje where IDDnia in (select IDDnia from Dni where IDKonferencji = @IDKonferencji))) and
								(Imie = @imie) and (Nazwisko = @nazwisko))
			if @uczestnikWlasciwy is not null begin
				-- musimy przepiac rezerwacje
				update Rezerwacje
				set IDUczestnika = @uczestnikWlasciwy
				where IDUczestnika = @uczestnik
				-- oraz usunac zbedny teraz wpis w tabeli Uczestnicy
				delete from Uczestnicy
				where IDUczestnika = @uczestnik
			end 
			else begin
				-- jesli jest unikalny, to wstawiamy odpowiednie wartosci i tylko sprawdzamy ew. znizke studencka
				update Uczestnicy
				set Imie = @imie, Nazwisko = @nazwisko, NrLegitymacjiStudenckiej = @nrlegitymacji
				where IDUczestnika = @uczestnik
			end

			if @nrlegitymacji is not null begin
				-- trzeba doliczyc znizke studencka
				set @znizkaStudencka = (select top 1 ZnizkaStudencka from Konferencje where IDKonferencji = @IDKonferencji)
				set @cenaDnia = (select top 1 Cena from Dni where IDDnia = @dzien)
				set @kwotaZnizki = @znizkaStudencka*@cenaDnia

				update Platnosci
				set KwotaZnizki = @kwotaZnizki
				where IDPlatnosci = (select top 1 IDPlatnosci from Platnosci where (IDKlienta = @IDKlienta) and (IDKonferencji = @IDKonferencji))

			end
			
			fetch next from @iterator into @dzien, @imie, @nazwisko, @nrlegitymacji
			
		end
		
		
		if @@ERROR != 0 begin raiserror('Cos poszlo nie tak. Pracujemy nad tym.', 1,1); rollback transaction end
			--fetch next from @iterator into @dzien, @imie, @nazwisko, @nrlegitymacji
		
		close @iterator
		deallocate @iterator
	commit transaction
end
go


/* -------------------------
   dodawanie/usuwanie zadan
   ------------------------- */

-- dodaje zadanie do tabeli Zadania
if object_id ('dodajZadanie', 'P') is not null
	drop procedure dodajZadanie;
go

create procedure dodajZadanie (
	@IDPlatnosci int,
	@TypZadania varchar(8)
) as
begin
	insert into Zadania
	values (@IDPlatnosci, @TypZadania)
end
go


-- usuwa zadanie
if object_id ('usunZadanie', 'P') is not null
	drop procedure usunZadanie;
go

create procedure usunZadanie (
	@IDZadania int
) as
begin
	delete from Zadania
	where IDZadania = @IDZadania
end
go


/* ------------------------------------------------
   procedury odpowiedzialne za pilnowanie terminow
   wywolywane codziennie
   ------------------------------------------------ */

-- usuwa wszystkie platnosci, ktore nie zostaly oplacone do tygodnia
-- od daty zlozenia zamowienia. jesli wplacono mniej, niz 50% kwoty dodajemy
-- do tabeli Zadania zadanie o identyfikatorze 'zaliczka'
create procedure usunPlatnosciPoTerminie
as begin
	declare @platnosc int,
			@iterNiezaplacone cursor,
			@iterNiedoplacone cursor

	set @iterNiezaplacone = cursor for (select IDPlatnosci from Platnosci
		where (dbo.get_date() > DATEADD(day, 7, DataZaksiegowania)) and (Zaplacone = 0))

	set @iterNiedoplacone = cursor for (select IDPlatnosci from Platnosci
		where (dbo.get_date() > DATEADD(day, 7, DataZaksiegowania)) and (Zaplacone < (0.5*(Kwota - KwotaZnizki))))

	-- usuwamy nieoplacone
	begin transaction
		open @iterNiezaplacone
		fetch next from @iterNiezaplacone into @platnosc
		while @@FETCH_STATUS = 0 begin
			exec usunPlatnosc @platnosc

			if @@ERROR != 0 begin raiserror('Nie udalo sie usunac platnosci', 1,1); rollback transaction end
			fetch next from @iterNiezaplacone into @platnosc
		end

		close @iterNiezaplacone
		deallocate @iterNiezaplacone
	commit transaction
	
	-- dla tych, ktore sa niedostatecznie oplacone ustawiamy Zadanie
	begin transaction
		open @iterNiedoplacone
		fetch next from @iterNiedoplacone into @platnosc
		while @@FETCH_STATUS = 0 begin
			exec dodajZadanie @platnosc, 'zaliczka' 

			if @@ERROR != 0 begin raiserror('Cos poszlo nie tak', 1,1); rollback transaction end
			fetch next from @iterNiedoplacone into @platnosc
		end

		close @iterNiedoplacone
		deallocate @iterNiedoplacone
	commit transaction

end
go


-- jesli pozostaly 2 tygodnie, a dalej mamy dla tego klienta osoby, ktore
-- nie maja wpisanych danych, dodajemy zadanie 'osoby' dla tej platnosci
if object_id ('prosOSzczegoly', 'P') is not null
	drop procedure prosOSzczegoly;
go

create procedure prosOSzczegoly
as begin
	declare @iterator cursor,
			@platnosc int,
			@brakuje bit

	set @iterator = cursor for (select IDPlatnosci from Platnosci)
	open @iterator
	fetch next from @iterator into @platnosc
	while @@FETCH_STATUS = 0 begin
		set @brakuje = (select dbo.czyBrakujeDanych(@platnosc))
		if @brakuje = 1 begin
			exec dodajZadanie @platnosc, 'osoby'
		end
		fetch next from @iterator into @platnosc
	end

	close @iterator
	deallocate @iterator
end


-- jesli znajduje platnosc, dla ktorej uplynal wiecej niz tydzien od konca
-- odpowiedniej konferencji, to dodaje rekord do tabeli Zadania z tagiem 'zaplata'
if object_id ('prosOZaplate', 'P') is not null
	drop procedure prosOZaplate;
go

create procedure prosOZaplate
as begin
	declare @iterator cursor,
			@platnosc int

	set @iterator = cursor for (
		select IDPlatnosci, IDKonferencji from Platnosci where ((
			select DataKonca from Konferencje where Konferencje.IDKonferencji = Platnosci.IDKonferencji
		) > DATEADD(day, -7, dbo.get_date()) and (Zaplacone < (Kwota - KwotaZnizki)))
	)

	open @iterator
	fetch next from @iterator into @platnosc
	while @@FETCH_STATUS = 0 begin
		exec dodajZadanie @platnosc, 'zaplata'
		fetch next from @iterator into @platnosc
	end

	close @iterator
	deallocate @iterator
end
go
-------------------------------------------------------------------------------
-- TWORZENIE TRIGGEROW
-------------------------------------------------------------------------------

-- przy dodawaniu konferencji sprawdza, czy DataStartu nie jest pozniejsza, niz DataKonca
create trigger dodawanieKonferencji
on Konferencje
after insert, update as
begin
	declare @IDKonferencji int,
			@DataStartu date,
			@DataKonca date
	set @IDKonferencji = (select IDKonferencji from inserted)
	set @DataStartu = (select DataStartu from inserted)
	set @DataKonca = (select DataKonca from inserted) 

	if @DataStartu > @DataKonca begin
		raiserror('DataStartu konferencji nie moze byc pozniejsza, niz data konca', 1,1)
		rollback transaction
	end
end
go

-- przy dodawaniu warsztatu sprawdza, czy warsztat nie przekracza ram czasowych konferencji
create trigger dodawanieWarszatu
on Warsztaty
after insert, update as
begin
	declare @IDWarsztatu int,
			@DataStartu date,
			@DataKonca date,
			@IDKonferencji int,
			@DataStartuKonf date,
			@DataKoncaKonf date
	
	set @IDWarsztatu = (select IDWarsztatu from inserted)
	set	@DataStartu = (select DataStartu from Warsztaty where IDWarsztatu = @IDWarsztatu)
	set	@DataKonca = (select DataKonca from Warsztaty where IDWarsztatu = @IDWarsztatu)
	set @IDKonferencji = (select IDKonferencji from Warsztaty where IDWarsztatu = @IDWarsztatu)
	set @DataStartuKonf = (select DataStartu from Konferencje where IDKonferencji = @IDKonferencji)
	set @DataKoncaKonf = (select DataKonca from Konferencje where IDKonferencji = @IDKonferencji)

	if (@DataStartu < @DataStartuKonf) begin
		raiserror('Warsztat musi zaczynac sie nie wczesniej, niz konferencja', 1,1)
		rollback transaction
	end

	if (@DataKonca > @DataKoncaKonf) begin
		raiserror('Warsztat musi konczyc sie nie pozniej, niz konferencja', 1,1)
		rollback transaction
	end
	
	if (@DataStartu > @DataKonca) begin
		raiserror('Warsztat musi zaczynac sie nie pozniej, niz sie konczy', 1,1)
		rollback transaction
	end
end
go


-- gdy dokonano wplaty na konferencje sprawdzamy, czy nie jest juz cala oplacona
-- (razem z warsztatami). jesli tak -> usuwamy obie platnosci
create trigger wplataRaty
on Platnosci
after update as
begin
	declare @IDPlatnosci int,
			@DoZaplacenia money,
			@Zaplacone money,
			@Znizka money

			
	-- top 1 sluzy li-tylko do dzialania generatora, w normalnym srodowisku nie bedzie potrzebne, bo
	-- kwerenda bedzie zwracac tylko 1 wynik
	set @IDPlatnosci = (select top 1 IDPlatnosci from inserted)
	set @DoZaplacenia = (select	top 1 Kwota from inserted)
	set @Zaplacone = (select top 1 Zaplacone from inserted)
	set @Znizka = (select top 1 KwotaZnizki from inserted)
	set @DoZaplacenia = @DoZaplacenia - @Znizka

	if @Zaplacone >= @DoZaplacenia begin
		declare @IDPlatnosciWarsztatu int,
				@DoZaplaceniaWarsztat money,
				@ZaplaconeWarsztat money,
				@IDKonferencji int,
				@IDKlienta int

		set  @IDKonferencji = (select IDKonferencji from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @IDKlienta = (select IDKlienta from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @IDPlatnosciWarsztatu = (select IDPlatnosciWarsztatu from PlatnosciWarsztatow
										where (IDKlienta = @IDKlienta) and (IDKonferencji = @IDKonferencji))
		set @DoZaplaceniaWarsztat = (select Kwota from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)
		set @ZaplaconeWarsztat = (select Zaplacone from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)

		-- jesli i konferencja, i warsztat sa oplacone, mozemy usunac cala platnosc.
		if @ZaplaconeWarsztat >= @DoZaplaceniaWarsztat begin
			-- TODO: moze wyslac jakiegos maila?
			begin transaction
				exec usunPlatnoscWarsztatu @IDPlatnosciWarsztatu
				if @@ERROR != 0 begin raiserror('Nie udalo sie sfinalizowac transakcji, sprawdz kwoty.', 1,1); rollback transaction end
				exec usunPlatnosc @IDPlatnosci
				if @@ERROR != 0 begin raiserror('Nie udalo sie sfinalizowac transakcji, sprawdz kwoty.', 1,1); rollback transaction end
			commit transaction
		end
	end
end
go

-- gdy dokonano wplaty na warsztat sprawdzamy, czy nie konferencja jest juz cala oplacona
-- jesli tak -> usuwamy obie platnosci
create trigger wplataRatyWarsztaty
on PlatnosciWarsztatow
after update as
begin
	declare @IDPlatnosciWarsztatu int,
			@DoZaplacenia money,
			@Zaplacone money

	set @IDPlatnosciWarsztatu = (select top 1 IDPlatnosciWarsztatu from inserted)
	set @DoZaplacenia = (select	top 1 Kwota from inserted)
	set @Zaplacone = (select top 1 Zaplacone from inserted)

	if @Zaplacone >= @DoZaplacenia begin
		declare @IDPlatnosci int,
				@DoZaplaceniaKonf money,
				@ZaplaconeKonf money,
				@ZnizkaKonf money,
				@IDKonferencji int,
				@IDKlienta int

		set  @IDKonferencji = (select IDKonferencji from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)
		set @IDKlienta = (select IDKlienta from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)
		set @IDPlatnosci = (select IDPlatnosci from Platnosci
										where (IDKlienta = @IDKlienta) and (IDKonferencji = @IDKonferencji))
		set @DoZaplaceniaKonf = (select Kwota from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @ZaplaconeKonf = (select Zaplacone from Platnosci where IDPlatnosci  = @IDPlatnosci)
		set @ZnizkaKonf = (select KwotaZnizki from Platnosci where IDPlatnosci = @IDPlatnosci)
		set @DoZaplaceniaKonf = @DoZaplaceniaKonf - @ZnizkaKonf

		-- jesli i konferencja, i warsztat sa oplacone, mozemy usunac cala platnosc.
		if @ZaplaconeKonf >= @DoZaplaceniaKonf begin
			-- TODO: moze wyslac jakiegos maila?
			begin transaction
				exec usunPlatnoscWarsztatu @IDPlatnosciWarsztatu
				if @@ERROR != 0 begin raiserror('Nie udalo sie sfinalizowac transakcji, sprawdz kwoty.', 1,1); rollback transaction end
				exec usunPlatnosc @IDPlatnosci
				if @@ERROR != 0 begin raiserror('Nie udalo sie sfinalizowac transakcji, sprawdz kwoty.', 1,1); rollback transaction end
			commit transaction
		end
	end
end
go


-- w momencie dodania Dnia do Konferencji sprawdza, czy dzien jest
-- faktycznie jednym z dni konferencji pod wzgl. daty
create trigger dodawanieDnia
on Dni
after insert, update as
begin
	declare @IDDnia int,
			@DataDnia date,
			@IDKonferecji int,
			@StartKonferencji date,
			@KoniecKonferencji date

	set @IDDnia = (select IDDnia from inserted)
	set @DataDnia = (select Data from inserted)
	set @IDKonferecji = (select IDKonferencji from inserted)
	set @StartKonferencji = (select DataStartu from Konferencje where IDKonferencji = @IDKonferecji)
	set @KoniecKonferencji = (select DataKonca from Konferencje where IDKonferencji = @IDKonferecji)

	if @DataDnia not between @StartKonferencji and @KoniecKonferencji begin
		raiserror('Data dnia musi miescic sie w konferencji', 1,1)
		rollback transaction
	end
end
go


-- przy dodawaniu dnia sprawdza, czy nie ma dwoch dni na ta sama date
create trigger dodanieDnia
on Dni
for insert, update as
begin
	declare @IDDnia int,
			@Data date,
			@IDKonferencji int

	set @IDDnia = (select IDDnia from inserted)
	set @Data = (select Data from inserted)
	set @IDKonferencji = (select IDKonferencji from Dni where IDDnia = @IDDnia)
	
	if exists (select * from Dni where (Data = @Data) and (IDDnia != @IDDnia) and (IDKonferencji = @IDKonferencji)) begin
		raiserror('Nie moze byc dwoch dni... tego samego dnia.', 1,1)
		rollback transaction
	end
end
go

-- w momencie dodawania progu upewnia się, ze nie ma dwoch progow o tej samej dacie granicznej
-- (na jedna konferencje)
-- w momencie tworzenia progu cenowego sprawdzamy, czy miesci sie w ramach konferencji
create trigger dodawanieProgu
on ProgiCenowe
after insert, update as
begin
	declare @IDProgu int,
			@DataGraniczna date,
			@IDKonferencji int,
			@StartKonferencji date,
			@KoniecKonferencji date

	set @IDProgu = (select IDProgu from inserted)
	set @DataGraniczna = (select DataGraniczna from inserted)
	set @IDKonferencji = (select IDKonferencji from ProgiCenowe where IDProgu = @IDProgu)
	set @StartKonferencji = (select DataStartu from Konferencje where IDKonferencji = @IDKonferencji)
	set @KoniecKonferencji = (select DataKonca from Konferencje where IDKonferencji = @IDKonferencji)

	
	if exists (select * from ProgiCenowe where (DataGraniczna = @DataGraniczna) and (IDProgu != @IDProgu) and (IDKonferencji = @IDKonferencji)) begin
		raiserror('Nie moze byc dwoch takich samych dat granicznych na jedna konferencje', 1,1)
		rollback transaction
	end
	
	if (@DataGraniczna < @StartKonferencji) or (@DataGraniczna > @KoniecKonferencji) begin
		raiserror('Prog musi miescic sie w ramach czasowych konferencji', 1,1)
		rollback transaction
	end
end
go


-- przy dodawaniu/zmienianiu danych uczestnika podmienia 0 na null w numerze legitymacji
-- (0 jest wynikiem dzialania parsera xml, a nie jest prawidlowym numerem)
create trigger dodawanieUczestnika
on Uczestnicy
after insert, update
as begin
	declare @IDUczestnika int,
			@NrLegitymacji int

	set @IDUczestnika = (select IDUczestnika from inserted)
	set @NrLegitymacji = (select NrLegitymacjiStudenckiej from inserted)

	if @NrLegitymacji = 0 begin
		update Uczestnicy
		set NrLegitymacjiStudenckiej = null
		where IDUczestnika = @IDUczestnika
	end
end


------------------------------------------------------------------------------
-- TWORZENIE WIDOKOW
------------------------------------------------------------------------------

-- wybiera 10 najczęstszych klientow w celu wyslania im
-- bonow upominkowych
if object_id ('najlepsiKlienci', 'V') is not null
	drop view najlepsiKlienci;
go

create view najlepsiKlienci
as
	select top 10 IDKlienta from ArchiwumPlatnosci
	group by IDKlienta order by COUNT(IDKlienta)
go

-- widok prezentujacy najbardziej popularne konferencje (10)
if object_id ('najlepszeKonferencje', 'V') is not null
	drop view najlepszeKonferencje;
go

create view najlepszeKonferencje
as
	select top 10 IDKonferencji from ArchiwumPlatnosci
	group by IDKonferencji order by count(*)
go


-- widok prezentujacy nieoplacone Platnosci
if object_id ('nieoplaconePlatnosci', 'V') is not null
	drop view nieoplaconePlatnosci;
go

create view nieoplaconePlatnosci
as
	select IDPlatnosci from Platnosci
	where Zaplacone <= 0.5*(Kwota - KwotaZnizki)
go

-- widok prezentujacy nieoplacone PlatnosciWarsztatow
if object_id ('nieoplaconePlatnosciWarsztatow', 'V') is not null
	drop view nieoplaconePlatnosciWarsztatow;
go

create view nieoplaconePlatnosciWarsztatow
as
	select IDPlatnosciWarsztatu from PlatnosciWarsztatow
	where Zaplacone <= Kwota
go


-- funkcja-widok prezentujaca listę uczestnikow na dana konferencje
if object_id ('listaNaKonferencje', 'F') is not null
	drop function listaNaKonferencje;
go

create function listaNaKonferencje ( @IDKonferencji int )
returns table
as
	return (select Uczestnicy.IDUczestnika, Imie, Nazwisko from Uczestnicy
		inner join Rezerwacje on Uczestnicy.IDUczestnika = Rezerwacje.IDUczestnika
		inner join Dni on Rezerwacje.IDDnia = Dni.IDDnia
		inner join Konferencje on Dni.IDKonferencji = Konferencje.IDKonferencji
		where Konferencje.IDKonferencji = @IDKonferencji
	)
go

-- funkcja-widok prezentujaca listę uczestnikow na dany warsztat
if object_id ('listaNaWarsztat', 'F') is not null
	drop function listaNaWarsztat;
go

create function listaNaWarsztat ( @IDWarsztatu int )
returns table
as
	return (select Uczestnicy.IDUczestnika, Imie, Nazwisko from Uczestnicy
		inner join RezerwacjeWarsztatow on Uczestnicy.IDUczestnika = RezerwacjeWarsztatow.IDUczestnika
		inner join Warsztaty on RezerwacjeWarsztatow.IDWarsztatu = Warsztaty.IDWarsztatu
		where Warsztaty.IDWarsztatu = @IDWarsztatu
	)
go

-------------------------------------------------------------------------
-- TWORZENIE FUNKCJI
-------------------------------------------------------------------------

-- sprawdza, czy dla danej platnosci trzeba uzupelnic dane Uczestnikow
if object_id ('czyBrakujeDanych', 'FN') is not null
	drop function czyBrakujeDanych;
go

create function czyBrakujeDanych (
	@IDPlatnosci int
) returns bit
as begin
	declare @uczestnik int,
			@konferencja int,
			@brakuje bit
	set @konferencja = (select IDKonferencji from Platnosci where IDPlatnosci = @IDPlatnosci)
	set @uczestnik = (select top 1 IDUczestnika from Uczestnicy
						where (Imie is null) and (Nazwisko is null) and (IDUczestnika in (select IDUczestnika from Rezerwacje
												where IDDnia in (select IDDnia from Dni where IDKonferencji = @konferencja))))
	if @uczestnik is not null
		set @brakuje = 1
	else
		set @brakuje = 0
	return @brakuje
end
go


-- funkcja pomocnicza -- sluzy do latwego oplacenia calej kwoty dla platnosci
if object_id ('oplac', 'F') is not null
	drop function oplac;
go

create function oplac( @IDPlatnosci int )
returns money
as begin
	declare @Kwota money,
			@KwotaZnizki money,
			@Zaplacone money

	set @Kwota = (select Kwota from Platnosci where IDPlatnosci = @IDPlatnosci)
	set @KwotaZnizki = (select KwotaZnizki from Platnosci where IDPlatnosci = @IDPlatnosci)
	set @Zaplacone = (select Zaplacone from Platnosci where IDPlatnosci = @IDPlatnosci)

	return (@Kwota - @KwotaZnizki - @Zaplacone)
end
go

-- analogicznie dla platnosci warsztatu
if object_id ('oplac', 'F') is not null
	drop function oplac;
go

create function oplacWarsztat( @IDPlatnosciWarsztatu int )
returns money
as begin
	declare @Kwota money,
			@Zaplacone money

	set @Kwota = (select Kwota from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)
	set @Zaplacone = (select Zaplacone from PlatnosciWarsztatow where IDPlatnosciWarsztatu = @IDPlatnosciWarsztatu)

	return (@Kwota - @Zaplacone)
end
go